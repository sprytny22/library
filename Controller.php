<?php

abstract class Controller {
    protected $view;
    protected $model;

    public function redirect($url) {
        header("location: ".$url);
    }
    public function loadView($name, $path='view/') {
        $file = $path.$name.'.php';
        if (file_exists($file)) {
            require $file;
            $model = new $file;
            return $model;
        }else {
            return null;
        }
    }
    public function loadModel($name, $path='model/') {
        $file = $path.$name.'.php';
        if (file_exists($file)) {
            require $file;
            $view = new $file;
            return $view;
        }else {
            return null;
        }
    }
}
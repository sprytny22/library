<?php

abstract class View {
    public function render($path_name){
        require 'view/header.php';
        require 'view/nav.php';
        require $path_name.'.php';
    }
}
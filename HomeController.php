<?php

class HomeController extends Controller {
    public function index()
    {
        $this->loadModel('home');
        $this->loadView('home');
    }
}
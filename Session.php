<?php
 class Session {
     public static function setSession($key, $value) {
         if ($key != NULL)
            $_SESSION[$key] = $value;
     }
     public static function getSession($key) {
         return $_SESSION[$key];
     }

     public static function unsetKey($key) {
         unset($_SESSION[$key]);
     }

     public static function unsetSession(){
         session_unset();
     }

     public static function startSession(){
         session_start();
     }
 }